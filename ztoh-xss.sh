#!/bin/bash

echo '[+] Make directory!'
echo ''
if [ ! -d "./target" ];
then
  mkdir ./target
fi

echo '[+] Running LinkScrapper And HTTPX tools'
echo ''
~/./tools/linkscrapper/linkScrapper.sh $1 | httpx -mc 200,404 -silent | sed 's/\?.*//g' | sort -u | anew ./target/$1.txt
echo '[+] Running ParamLeaks tool'
echo ''
paramleaks -l ./target/$1.txt -s | anew ./target/$1_params.txt
echo '[+] Running x8 tool'
echo ''
x8 -u ./target/$1.txt -w ./target/$1_params.txt --reflected-only --follow-redirects --mimic-browser -c 3 -m 25 -d 2000 --disable-progress-bar -O url -o ./x8_result_$1.txt
echo 'Happy Hacker :)'
